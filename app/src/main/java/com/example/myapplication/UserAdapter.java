package com.example.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class UserAdapter extends ArrayAdapter<User>{

    private Context context;
    private ArrayList<User> userList;

    public UserAdapter(@NonNull Context context, ArrayList<User> userList) {
        super(context, 0,userList);
        this.context = context;
        this.userList = userList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;

        if (row == null) {
            row = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        }

        User user = userList.get(position);

        if (row != null) {
            TextView textViewName = row.findViewById(R.id.textView_name);
            TextView textViewEmail = row.findViewById(R.id.textView_email);

            textViewName.setText(user.getName());
            textViewEmail.setText(user.getEmail());
        }

        return row;
    }
}
